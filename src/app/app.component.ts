import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  data = [
    {
      title: '1',
      children: [
        { title: '1-1', children: null },
        {
          title: '1-2', children: [
            { title: '1-2-1', children: null },
            { title: '1-2-2', children: null },
          ]
        },
        { title: '1-3', children: null },
      ]
    },
    {
      title: '2',
      children: [
        { title: '2-1', children: null },
        { title: '2-2', children: null },
      ]
    }
  ]
}
